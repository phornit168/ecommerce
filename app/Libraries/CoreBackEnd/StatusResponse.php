<?php


namespace App\Libraries\CoreBackEnd;


use App\Http\Resources\sendCollection;

trait StatusResponse
{
    public function Response($data, $statusCode, $message){
        $response = [
            'data'    =>  $data ,
            'statusCode' => $statusCode,
            'message' => $message,
        ];
        return response()->json($response, $statusCode);
    }

    public function ResponseList($data){
        return new sendCollection($data);
    }
}
