<?php

namespace App\Models\CoreBackEnd;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "country";
    protected $fillable = [
        'code',
        'name',
        'created_at',
        'updated_at'
    ];
}
