<?php

namespace App\Http\Middleware;

use App\Libraries\CoreBackEnd\StatusResponse;
use Closure;

class AuthKey
{
    use StatusResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-API-KEY');
        if ($token != '1ccbc4c913bc4ce785a0a2de444aa0d6'){
            return $this->Response([], 401, "Api key not found");
        }
        return $next($request);
    }
}
