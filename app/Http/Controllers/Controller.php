<?php

namespace App\Http\Controllers;

use App\Http\Resources\sendCollection;
use App\Libraries\CoreBackEnd\StatusResponse;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use Illuminate\Routing\Controller as BaseController;

class Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, StatusResponse;

    public function sendListResponse($data)
    {
        $response = $this->ResponseList($data);
        return $response;
    }

    public function sendResponse($data, $statusCode = 200 , $message = 'Success')
    {
        return $this->Response($data, $statusCode, $message);
    }

    public function sendNotFoundResponse($message = 'Record Not Found')
    {
        $response = $this->Response(null, 404, $message);
        return response()->json($response, 404);
    }

    public function sendInvalidResponse($message = 'Invalid request')
    {
        $response = $this->Response(null, 400, $message);
        return response()->json($response, 400);
    }
}
