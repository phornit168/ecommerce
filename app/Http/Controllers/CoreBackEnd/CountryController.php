<?php

namespace App\Http\Controllers\CoreBackEnd;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\countryCollection;
use App\Http\Resources\sendCollection;
use App\Http\Resources\sendResource;
use App\Models\CoreBackEnd\Country;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CountryController extends Controller
{

//    /**
//     * Section name to be displayed when deleting items
//     * @var string
//     */
//    protected $section = 'country';
//
//    /**
//     * The model to use when querying/deleting
//     * @var [type]
//     */
//    protected $model = Country::class;
//
//    /**
//     * Relationships to allow users to include in queries
//     *
//     * @var array
//     */
//    protected $includes = [
//        'code',
//        'name',
//    ];
//
//    /**
//     * Allowed filters for users
//     *
//     * @var array
//     */
//    protected $filters = [
//        'code',
//        'name',
//    ];
//
//    /**
//     * API Resource collection
//     *
//     * @var [type]
//     */
//    protected $resourceCollection = countryCollection::class;



    /**
     * Display a listing of the resource.
     *
     * @return sendCollection
     */
    public function index()
    {
        $data = Country::paginate(10);
        return $this->sendListResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'name' => 'required'
        ]);

        $country = Country::create($request->all());
        return $this->sendResponse($country);
    }

    /**
     * Display the specified resource.
     *
     * @param Country $country
     * @return sendResource
     */
    public function show(Country $country)
    {
        return $this->sendResponse($country);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Country $country
     * @param \Illuminate\Http\Request $request
     * @return sendResponse
     */
    public function update(Country $country, Request $request)
    {
        $country ->update($request->all());
        return $this->sendResponse($country);
    }

    /**
     * Remove the specified resource from storage.
     * @param Country $country
     * @return Response
     */
    public function destroy(Country $country)
    {
        $country->delete();
        return $this->sendResponse($country);
    }
}
