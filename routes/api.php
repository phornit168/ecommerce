<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

$arr_core =	[
    "country" => "countryController",
    "city" => "cityController"
];

foreach ($arr_core as $key => $value)
{
    Route::apiResource($key, 'CoreBackEnd\\'.$value,['only' => ['index', 'create', 'store', 'edit', 'update','show']]);
}

Route::post('register', 'Auth\UserController@register');
Route::post('login', 'Auth\UserController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'Auth\UserController@getAuthenticatedUser');
});
